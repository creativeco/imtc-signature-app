class CreateMailsignatures < ActiveRecord::Migration[5.0]
  def change
    create_table :mailsignatures do |t|
      t.string :name
      t.string :saludation
      t.string :mobilespain
      t.boolean :msvisible
      t.string :mobilefrance
      t.boolean :mfvisible
      t.string :mobilegermany
      t.boolean :mgvisible
      t.string :skype
      t.string :facebook
      t.string :twitter
      t.integer :banner
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end

require 'test_helper'

class MailsignaturesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @mailsignature = mailsignatures(:one)
  end

  test "should get index" do
    get mailsignatures_url
    assert_response :success
  end

  test "should get new" do
    get new_mailsignature_url
    assert_response :success
  end

  test "should create mailsignature" do
    assert_difference('Mailsignature.count') do
      post mailsignatures_url, params: { mailsignature: { banner: @mailsignature.banner, facebook: @mailsignature.facebook, mfvisible: @mailsignature.mfvisible, mgvisible: @mailsignature.mgvisible, mobilefrance: @mailsignature.mobilefrance, mobilegermany: @mailsignature.mobilegermany, mobilespain: @mailsignature.mobilespain, msvisible: @mailsignature.msvisible, name: @mailsignature.name, saludation: @mailsignature.saludation, skype: @mailsignature.skype, twitter: @mailsignature.twitter, user_id: @mailsignature.user_id } }
    end

    assert_redirected_to mailsignature_url(Mailsignature.last)
  end

  test "should show mailsignature" do
    get mailsignature_url(@mailsignature)
    assert_response :success
  end

  test "should get edit" do
    get edit_mailsignature_url(@mailsignature)
    assert_response :success
  end

  test "should update mailsignature" do
    patch mailsignature_url(@mailsignature), params: { mailsignature: { banner: @mailsignature.banner, facebook: @mailsignature.facebook, mfvisible: @mailsignature.mfvisible, mgvisible: @mailsignature.mgvisible, mobilefrance: @mailsignature.mobilefrance, mobilegermany: @mailsignature.mobilegermany, mobilespain: @mailsignature.mobilespain, msvisible: @mailsignature.msvisible, name: @mailsignature.name, saludation: @mailsignature.saludation, skype: @mailsignature.skype, twitter: @mailsignature.twitter, user_id: @mailsignature.user_id } }
    assert_redirected_to mailsignature_url(@mailsignature)
  end

  test "should destroy mailsignature" do
    assert_difference('Mailsignature.count', -1) do
      delete mailsignature_url(@mailsignature)
    end

    assert_redirected_to mailsignatures_url
  end
end

class MailsignaturesController < ApplicationController
  before_action :set_mailsignature, only: [:show, :edit, :update, :destroy]

  # GET /mailsignatures
  # GET /mailsignatures.json
  def index
    @mailsignature = current_user.mailsignature
  end

  # GET /mailsignatures/1
  # GET /mailsignatures/1.json
  def show
  end

  # GET /mailsignatures/new
  def new
    @mailsignature = Mailsignature.new
  end

  # GET /mailsignatures/1/edit
  def edit
  end

  # POST /mailsignatures
  # POST /mailsignatures.json
  def create
    @mailsignature = Mailsignature.new(mailsignature_params)
    @mailsignature.user=current_user
    respond_to do |format|
      if @mailsignature.save
        format.html { redirect_to mailsignatures_url, notice: 'Mailsignature was successfully created.' }
        format.json { render :show, status: :created, location: @mailsignature }
      else
        format.html { render :new }
        format.json { render json: @mailsignature.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /mailsignatures/1
  # PATCH/PUT /mailsignatures/1.json
  def update
    respond_to do |format|
      if @mailsignature.update(mailsignature_params)
        format.html { redirect_to mailsignatures_url, notice: 'Mailsignature was successfully updated.' }
        format.json { render :show, status: :ok, location: @mailsignature }
      else
        format.html { render :edit }
        format.json { render json: @mailsignature.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /mailsignatures/1
  # DELETE /mailsignatures/1.json
  def destroy
    @mailsignature.destroy
    respond_to do |format|
      format.html { redirect_to mailsignatures_url, notice: 'Mailsignature was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_mailsignature
      @mailsignature = Mailsignature.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def mailsignature_params
      params.require(:mailsignature).permit(:name, :saludation, :mobilespain, :msvisible, :mobilefrance, :mfvisible, :mobilegermany, :mgvisible, :skype, :skypevisible,:facebook, :twitter, :banner, :user_id)
    end
end
